require 'bundler/setup'
require 'nokogiri'
require 'json'

def hello(event:, context:)
  {
    statusCode: 200,
    body: {
      message: 'success!',
      input: event
    }.to_json
  }
end
